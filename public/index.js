/**
 * Created by Tan Seng Ghee on 20/10/16.
 */

(function(){
    var regApp = angular.module("regApp", []);
    var RegCtrl = function() {
        var regCtrl = this;


        regCtrl.member = {
            email: "",
            pwd: "",
            name: "",
            gender: "",
            dob: "",
            address: "",
            country: "",
            number: "",
            year: ""
        };

        regCtrl.members = [ ];

        regCtrl.submit = function (member) {
            regCtrl.members.push(member);
            console.log(regCtrl.members);
        };



        regCtrl.resetEntry = function () {
            regCtrl.member.email = " ",
            regCtrl.member.pwd = " ",
            regCtrl.member.name = " ",
            regCtrl.member.gender = " ",
            regCtrl.member.dob = " ",
            regCtrl.member.address = " ",
            regCtrl.member.country = " ",
            regCtrl.member.number = " "
            console.log(regCtrl.members);
        }
    };
    regApp.controller("RegCtrl", [RegCtrl]);

})();